#### 介绍
慕课—Node.js开发仿知乎服务端 深入理解RESTful API

# 7 上传图片页面访问路径
    http://localhost:3306/upload.html

# 6 图片访问路径
    http://localhost:3306/uploads/upload_232cbc7cc19edf631c399348cd0e814e.jpeg

# 5 自己编写koa中间件时间用户认证与授权
    const auth = async (ctx, next) => {
        const { authorization = '' } = ctx.request.header
        const token = authorization.replace('Bearer', '')
    
        try {
            // 校验密钥是否正确
            const user = jsonwebtoken.verify(token, secret)
            ctx.state.user = user
        } catch (err) {
            ctx.throw(401, err.message)
        }
        await next()
    }

# 4 postmant设置全局token
    var jsonData = pm.response.json();
    pm.globals.set("token", jsonData.``token);

# 3 cross-env windows环境，用于区分环境
    cross-env NODE_ENV=production

# 2 自己编写错误处理中间件
    app.use(async (ctx, next) => {
        try {
            await next()
        } catch (err) {
            ctx.status = err.status || err.statusCode || 500
            ctx.body = {
                message: err.message
            }
        }
    })

# 1 运行
    npm run start 生产环境
    npm run dev 开发环境