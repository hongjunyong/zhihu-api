/**
 *
 * @description: 用户
 * @author: junyong.hong
 * @createTime: 2019/9/27
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const mongoose = require('mongoose')
const { Schema, model } = mongoose

const userSchema = new Schema({
    __v: {
        type: Number,
        select: false
    },
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 头像
    avatar_url: {
        type: String
    },
    // 性别
    gender: {
        type: String,
        enum: ['male', 'female'],
        default: 'male',
        required: true
    },
    // 一句话介绍
    headline: {
        type: String
    },
    // 居住地
    locations: {
        // 是一个数组
        type: [{ type: Schema.Types.ObjectId, ref: 'Topic' }],
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 行业
    business: {
        type: Schema.Types.ObjectId,
        ref: 'Topic',
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 职业经历
    employments: {
        type: [{
            company: { type: Schema.Types.ObjectId, ref: 'Topic' },
            job: { type: Schema.Types.ObjectId, ref: 'Topic' }
        }],
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 教育经历
    educations: {
        type: [{
            // 学校
            scholl: { type: Schema.Types.ObjectId, ref: 'Topic' },
            // 专业
            major: { type: Schema.Types.ObjectId, ref: 'Topic' },
            // 学历
            diploma: { type: Number, enum: [1, 2, 3, 4, 5] },
            // 入学年份
            entrance_year: { type: Number },
            // 毕业年份
            graduation_year: { type: Number }
        }],
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 关注
    following: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'User'
        }],
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 关注话题
    followingTopics: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Topic'
        }],
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 赞
    likingAnswers: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Answer'
        }],
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 踩
    dislikingAnswers: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Answer'
        }],
        // 获取列表，改字段不会被显示出来
        select: false
    },
    // 收藏答案
    collectingAnswers: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Answer'
        }],
        // 获取列表，改字段不会被显示出来
        select: false
    }
}, { timestamps: true })

module.exports = model('User', userSchema)