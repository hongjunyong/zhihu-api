/**
 *
 * @description: 问题
 * @author: junyong.hong
 * @createTime: 2019/10/15
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const mongoose = require('mongoose');

const { Schema, model } = mongoose;
const questionSchema = new Schema({
    __v: { type: Number, select: false },
    title: { type: String, required: true },
    // 描述
    description: { type: String },
    // 提问者
    questioner: { type: Schema.Types.ObjectId, ref: 'User', required: true, select: false },
    topics: {
        type: [{ type: Schema.Types.ObjectId, ref: 'Topic' }],
        select: false,
    },
}, { timestamps: true });

module.exports = model('Question', questionSchema);