/**
 *
 * @description: 评论
 * @author: junyong.hong
 * @createTime: 2019/10/22
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const mongoose = require('mongoose');

const { Schema, model } = mongoose;
const commentSchema = new Schema({
    __v: { type: Number, select: false },
    // 内容
    content: { type: String, required: true },
    // 评论人
    commentator: { type: Schema.Types.ObjectId, ref: 'User', required: true, select: false },
    // 从属于哪个问题
    questionId: { type: String, required: true },
    // 回答id
    answerId: { type: String, required: true },
    // 根评论的id
    rootCommentId: { type: String },
    // 回复给哪个用户
    replyTo: { type: Schema.Types.ObjectId, ref: 'User' },
}, { timestamps: true });

module.exports = model('Comment', commentSchema);