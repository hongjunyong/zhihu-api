/**
 *
 * @description: 答案
 * @author: junyong.hong
 * @createTime: 2019/10/21
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const mongoose = require('mongoose');

const { Schema, model } = mongoose;
const answerSchema = new Schema({
    __v: { type: Number, select: false },
    // 内容
    content: { type: String, required: true },
    // 回答者
    answerer: { type: Schema.Types.ObjectId, ref: 'User', required: true, select: false },
    // 从属于哪个问题
    questionId: { type: String, required: true },
    // 投票数
    voteCount: { type: Number, required: true, default: 0 },
}, { timestamps: true });

module.exports = model('Answer', answerSchema);