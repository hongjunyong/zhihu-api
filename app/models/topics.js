/**
 *
 * @description: 话题
 * @author: junyong.hong
 * @createTime: 2019/9/27
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const mongoose = require('mongoose')
const { Schema, model } = mongoose

const TopicSchema = new Schema({
    __v: {
        type: Number,
        select: false
    },
    // 标题
    name: {
        type: String,
        required: true
    },
    // 话题图片
    avatar_url: {
        type: String
    },
    // 简介
    introduction: {
        type: String,
        select: false
    }
}, { timestamps: true })

module.exports = model('Topic', TopicSchema)