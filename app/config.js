/**
 *
 * @description:
 * @author: junyong.hong
 * @createTime: 2019/9/27
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
module.exports = {
    secret: 'zhihu-jwt-secret',
    connectionStr: 'mongodb://localhost/zhihu-api'
}