/**
 *
 * @description: 评论
 * @author: junyong.hong
 * @createTime: 2019/10/22
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const jwt = require('koa-jwt');
const Router = require('koa-router');
// /questions/问题id/answers/回答（答案）id/comments
const router = new Router({ prefix: '/questions/:questionId/answers/:answerId/comments' });
const {
    find, findById, create, update, delete: del,
    checkCommentExist, checkCommentator,
} = require('../controllers/comments');

const { secret } = require('../config');

const auth = jwt({ secret });

// 查找评论列表
router.get('/', find);
// 创建评论
router.post('/', auth, create);
// 查找特定的评论
router.get('/:id', checkCommentExist, findById);
// 修改评论
router.patch('/:id', auth, checkCommentExist, checkCommentator, update);
// 删除评论
router.delete('/:id', auth, checkCommentExist, checkCommentator, del);

module.exports = router;