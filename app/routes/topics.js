/**
 *
 * @description:
 * @author: junyong.hong
 * @createTime: 2019/9/25
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const jwt = require('koa-jwt')
const Router = require('koa-router')
const router = new Router({
    prefix: '/topics'
})
const {
    find,
    findById,
    create,
    update,
    listTopicFollowing,
    checkTopicExist,
    listQuestions
} = require('../controllers/topics')

const { secret } = require('../config')

// 认证中间件
const auth = jwt({ secret })

// 查询所有话题
router.get('/', find)
// 创建话题
router.post('/', auth, create)
// 查询指定话题
router.get('/:id', checkTopicExist, findById)
// 修改话题
router.patch('/:id', auth, checkTopicExist, update)
// 获取某个话题的粉丝列表
router.get('/:id/followers', auth, checkTopicExist, listTopicFollowing)

// 获取某个话题的问题列表
router.get('/:id/questions', auth, checkTopicExist, listQuestions)

module.exports = router