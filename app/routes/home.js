/**
 *
 * @description:
 * @author: junyong.hong
 * @createTime: 2019/9/25
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const Router = require('koa-router')
const router = new Router()
const { index, upload } = require('../controllers/home')

router.get('/', index)
router.post('/upload', upload)

module.exports = router
