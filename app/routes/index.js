/**
 *
 * @description: 加载路由
 * @author: junyong.hong
 * @createTime: 2019/9/25
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const fs = require('fs')
module.exports = (app) => {
    fs.readdirSync(__dirname).forEach(file => {
        if (file === 'index.js') {
            return
        }

        const route = require(`./${file}`)
        app.use(route.routes()).use(route.allowedMethods())
    })
}