/**
 *
 * @description:
 * @author: junyong.hong
 * @createTime: 2019/10/21
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const jwt = require('koa-jwt');
const Router = require('koa-router');
const router = new Router({ prefix: '/questions/:questionId/answers' });
const {
    find,
    findById,
    create,
    update,
    delete: del,
    checkAnswerExist, checkAnswerer,
} = require('../controllers/answers');

const { secret } = require('../config');

const auth = jwt({ secret });

// 答案列表
router.get('/', find);
// 创建答案
router.post('/', auth, create);
// 获取特定答案 questions/问题id/answers/答案id
router.get('/:id', checkAnswerExist, findById);
// 修改答案
router.patch('/:id', auth, checkAnswerExist, checkAnswerer, update);
// 删除答案
router.delete('/:id', auth, checkAnswerExist, checkAnswerer, del);

module.exports = router;