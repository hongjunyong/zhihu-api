/**
 *
 * @description:
 * @author: junyong.hong
 * @createTime: 2019/9/25
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const jwt = require('koa-jwt')
const Router = require('koa-router')
const router = new Router({
    prefix: '/users'
})
const {
    find, findById, create, update,
    delete: del, login, checkOwner,
    listFollowing, listFollowers,
    checkUserExist, follow, unfollow,
    listFollowersTopics, followTopic, unfollowTopic,
    listQuestions,
    listLikingAnswers, likeAnswer, unlikeAnswer,
    listDislikingAnswers, dislikeAnswer, undislikeAnswer,
    listCollectingAnswers, collectAnswer, uncollectAnswer,
} = require('../controllers/users')

const { checkTopicExist } =  require('../controllers/topics')
// 检查答案是否存在
const { checkAnswerExist } = require('../controllers/answers');

const { secret } = require('../config')

// 认证中间件
const auth = jwt({ secret })

// 获取用户列表
router.get('/', auth, find)
// 创建用户
router.post('/', create)
// 获取特定用户
router.get('/:id', auth, checkOwner, findById)
// 修改用户 put整体替换  patch更新部分
router.patch('/:id', auth, checkOwner, update)
// 删除用户
router.delete('/:id', del)

// 登录（获取token）
router.post('/login', login)

// 获取关注者与粉丝
router.get('/:id/following', listFollowing)
// 关注某人
router.put('/following/:id', auth, checkUserExist, follow)
// 取消关注
router.delete('/following/:id', auth, checkUserExist, unfollow)
// 获取某个用户的粉丝列表
router.get('/:id/followers',  listFollowers)

// 获取话题列表
router.get('/:id/followingTopics', listFollowersTopics)
// 关注话题
router.put('/followingTopics/:id', auth, checkTopicExist, followTopic)
// 取消关注话题
router.delete('/followingTopics/:id', auth,checkTopicExist, unfollowTopic)

// 话题
router.get('/:id/questions', listQuestions);

// 获取某个用户赞的答案
router.get('/:id/likingAnswers', listLikingAnswers);
// 赞答案（如果赞了，踩就去掉）
router.put('/likingAnswers/:id', auth, checkAnswerExist, likeAnswer, undislikeAnswer);
// 取消赞答案
router.delete('/likingAnswers/:id', auth, checkAnswerExist, unlikeAnswer);

// 获取某个用户踩的答案
router.get('/:id/dislikingAnswers', listDislikingAnswers);
// 踩答案（如果踩了，赞就去掉）
router.put('/dislikingAnswers/:id', auth, checkAnswerExist, dislikeAnswer, unlikeAnswer);
// 取消踩答案
router.delete('/dislikingAnswers/:id', auth, checkAnswerExist, undislikeAnswer);

// 列出收藏的答案
router.get('/:id/collectingAnswers', listCollectingAnswers);
// 收藏答案
router.put('/collectingAnswers/:id', auth, checkAnswerExist, collectAnswer);
// 取消收藏答案
router.delete('/collectingAnswers/:id', auth, checkAnswerExist, uncollectAnswer);

module.exports = router