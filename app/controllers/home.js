/**
 *
 * @description:
 * @author: junyong.hong
 * @createTime: 2019/9/25
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const path = require('path')

class HomeCtl {
    index (ctx) {
        ctx.body = 'hjy'
    }

    // 上传文件
    upload(ctx) {
        const file = ctx.request.files.file
        const basename = path.basename(file.path)
        ctx.body = {
            url: `${ctx.origin}/uploads/${basename}`
        }
    }
}

module.exports = new HomeCtl()