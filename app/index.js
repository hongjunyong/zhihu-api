/**
 *
 * @description:
 * @author: junyong.hong
 * @createTime: 2019/9/21
 * @version: 1.0.0.0
 * @history:
 *    1、
 *    2、
 *
 */
const Koa = require('koa')
const koaBody = require('koa-body')
// 生成图片链接
const koaStatic = require('koa-static')
// 处理错误信息
const error = require('koa-json-error')
// 校验参数
const parameter = require('koa-parameter')
const mongoose = require('mongoose')
const path = require('path')
const app = new Koa()
const routing = require('./routes/index')
const { connectionStr } = require('./config')

mongoose.connect(connectionStr, () => {
    console.log('MongoDB 连接成功~')
})
mongoose.connection.on('error', console.error)

// 设置图片访问路径
app.use(koaStatic(path.join(__dirname, 'public')))

// koa-json-error处理错误信息
app.use(error({
    // 生产环境不返回错误信息（堆栈信息）
    postFormat: (e, {stack, ...rest}) => process.env.NODE_ENV === 'production' ? rest : { stack, ...rest }
}))

app.use(koaBody({
    // 支持文件
    multipart: true,
    formidable: {
        // 上传目录
        uploadDir: path.join(__dirname, '/public/uploads'),
        // 保留文件后缀名称
        keepExtensions: true
    }
}))
app.use(parameter(app))
routing(app)

app.listen(3306, () => {
    console.log('程序启动在 3306 端口了')
})